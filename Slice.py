
def getArea(fro,to):
    return (to[1] - fro[1]) * (to[0] - fro[0])

class Slice:
    def __init__(self,pizza,seedFrom,seedTo):
        self.pizza = pizza
        self.fro = seedFrom
        self.to = seedTo
        self.ammissibile = False
        self.area = getArea(self.fro,self.to)

    def extend(self,move):
        if "u"==move:
            self.fro=(self.fro[0]-1,self.fro[1])
            self.area += self.to[1] -self.fro[1]
        elif 'd'==move:
            self.to=(self.to[0]+1,self.to[1] )
            self.area += self.to[1] -self.fro[1]
        elif 'l'==move:
            self.fro=(self.fro[0],self.fro[1]-1)
            self.area += self.to[0] -self.fro[0]
        elif 'r'==move:
            self.to=(self.to[0],self.to[1]+1)
            self.area += self.to[0] -self.fro[0]



    '''
    Ho una funzione per valutare quale sia la "migliore mossa"
    '''
    def varphi( self, move):
        '''
        0 se non è ammissibile,
        1 se contiene una pizza
        2 se può aumentare senza soddisfare il vincolo
        3 se migliorare porta a soddisfare il vincolo

        :param pizza:
        :param slice:
        :param move:
        :return:
        '''
        nextFrom = self.fro
        nextTo = self.to

        if "u" == move:
            nextFrom = (nextFrom[0] - 1, nextFrom[1])
        elif 'd' == move:
            nextTo = (nextTo[0] + 1, nextTo[1])
        elif 'l' == move:
            nextFrom = (nextFrom[0], nextFrom[1] - 1)
        elif 'r' == move:
            nextTo = (nextTo[0], nextTo[1] + 1)

        #ammissibilità del bordo con ipotesi di
        if nextFrom[0] < 0 or nextFrom[1] < 0 or nextTo[0] > self.pizza.row_count or nextTo[1] > self.pizza.col_count:
            return 0

        #ammissibilità dimensione senza fusione
        if getArea(nextFrom,nextTo)>self.pizza.max_area:
            return 0

        #tipo di elementi nell'estensione con IPOTESI di ammissibilità bordo
        extElement = []
        extElementPossiblePizza = []

        if "u" == move:
            extElement = self.pizza.grid[nextFrom[0],nextFrom[1]:nextTo[1]]
            extElementPossiblePizza = self.pizza.bitmap[nextFrom[0],nextFrom[1]:nextTo[1]]
        elif 'd' == move:
            extElement = self.pizza.grid[nextTo[0]-1,nextFrom[1]:nextTo[1]]
            extElementPossiblePizza = self.pizza.bitmap[nextTo[0]-1,nextFrom[1]:nextTo[1]]
        elif 'l' == move:
            extElement = self.pizza.grid[nextFrom[0]:nextTo[0],nextFrom[1]]
            extElementPossiblePizza = self.pizza.bitmap[nextFrom[0]:nextTo[0],nextFrom[1]]
        elif 'r' == move:
            extElement= self.pizza.grid[nextFrom[0]:nextTo[0],nextTo[1]-1]
            extElementPossiblePizza = self.pizza.bitmap[nextFrom[0]:nextTo[0],nextTo[1]-1]

        if any([isinstance(el, Slice) for el in extElementPossiblePizza]):
            return 1

        elementoGettonato = False if self.pizza.min_el==True else True

        if any([ elementoGettonato==el for el in extElement ]):
            return 2#*self.squareMultiplier(mossa=move,alpha=0.7)

        return 2*self.squareMultiplier(mossa=move,alpha=0.7)

    def squareMultiplier(self,mossa,alpha):
        if(mossa in "ud"):
            return 1 if (self.to[0] - self.fro[0]) < (self.to[1] - self.fro[1]) else alpha
        else:
            return 1 if (self.to[0] - self.fro[0]) > (self.to[1] - self.fro[1]) else alpha
    def isFull(self):
        return self.area==self.pizza.max_area


    def isValidForMinIngr(self):
        count1 = 0
        count2 = 0
        for x in range(self.fro[0],self.to[0]):
            for y in range(self.fro[1],self.to[1]):
                if self.pizza.grid[x,y] == True:
                    count1+=1
                else:
                    count2+=1
        return (count1>= self.pizza.min_ingr) and (count2>= self.pizza.min_ingr) and self.area <=self.pizza.max_area


'''
    def splitOrMerge(self):
        neighbor = []
        if self.fro[0]>1:
            neighbor.append( self.pizza.bitmap[self.fro[0]-1,self.fro[1]] )
        if self.fro[1]>1:
            neighbor.append( self.pizza.bitmap[self.fro[0],self.fro[1]-1] )
        if self.to[0] < self.pizza.row_count:
            neighbor.append(self.pizza.bitmap[self.fro[0], self.fro[1]])
            #todo manca

'''