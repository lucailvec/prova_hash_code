from collections import namedtuple
from collections import Counter
import numpy as np

import Slice
import sys
import re


def getPizza(file):
    file = open(file, "r")
    row_count, col_count, min_ingr, max_area = tuple(file.readline().split(" "))
    return Pizza(int(row_count), int(col_count), int(min_ingr), int(max_area), file.read()[:-1])


class Pizza:

    def __init__(self, row_count, col_count, min_ingr, max_area,textNotFormatted):
        self.row_count= row_count
        self.col_count = col_count
        self.min_ingr = min_ingr
        self.max_area = max_area
        self.grid = np.empty(shape= (row_count,col_count),dtype=bool)

        textNotFormatted = textNotFormatted.replace("\n","")
        for y in range(0,col_count):
            for x in range(0,row_count):
                self.grid[x,y]= 0 if textNotFormatted[x*col_count + y]=="T" else 1
        self.bitmap = np.zeros(shape=(row_count,col_count),dtype=Slice.Slice)

        self.tot_el = col_count*row_count

        self.min_el = True if np.sum(self.grid)<self.tot_el - np.sum(self.grid) else False
        self.num_min_el = np.sum(self.grid) if np.sum(self.grid)<self.tot_el - np.sum(self.grid) else self.tot_el - np.sum(self.grid)
        self.seedSlice = []

        divider=1
        stima = self.col_count * self.row_count // self.min_ingr
        if stima >  5 and stima <45:
            divider =3
        elif stima < 13000:
            divider = 8
        else:
            divider = 80


        for y in range(0, col_count):
            for x in range(0, row_count):
                if self.grid[x,y]==self.min_el and np.random.choice(divider)==0:
                    self.seedSlice.append(Slice.Slice(self,(x,y),(x+1,y+1)))



        [self.updateBitmap(el) for el in self.seedSlice]


    '''
    Prende uno Slice(da,a) di pizza e occupa la bitmap.
    Fare controllo sul non riempire un punto già riempito o no ?  
    '''
    def updateBitmap(self,slice):
        for x in range(slice.fro[0],slice.to[0]):
            for y in range(slice.fro[1],slice.to[1]):
                if type(self.bitmap[x,y]) is Slice.Slice and not self.bitmap[x,y] == slice:
                    #raise BaseException
                    #todo print("la migliore mossa è bho")
                    pass
                else:
                    self.bitmap[x,y]=slice


    def bestMove(self,phi_moves): # { "l":2, ...}

        prob = []
        ms = []
        for k,v in phi_moves.items():
            ms.append(k)
            prob.append(0 if v==1 else v) #todo continua da qua

        prob = np.asarray(prob)/sum(prob)
        return np.random.choice(np.asarray(ms),p=prob)

    def step(self):
        flag =0
        willBeRemoved = []
        for s in self.seedSlice:
            #if not s.isFull():
            moves = "udlr"
            phi_moves = {m:s.varphi(m) for m in moves}

            if any([ el >1 for el in phi_moves.values()]):#todo attento cche dovrai toglierlo quando imlpimenti l'unione di pià slice
                bestMove = self.bestMove(phi_moves)
                s.extend(bestMove)
                self.updateBitmap(s)
            else:
                if s.area==1:
                    self.unupdateBitmap(s)
                    willBeRemoved.append(s)
                flag+=1

        #todo controllare la rimozione mmm
        [ self.seedSlice.remove(el) for el in willBeRemoved]
        if flag ==len(self.seedSlice):#se non ho avuto che la migliore mossa era quella di fondere
            raise BaseException

    def __str__(self):
        return "{}\n{}\n{}\n{}".format("Pizza",self.grid ,"Bitmap",self.bitmap)

    def isFull(self):
        return all([all([isinstance(a, Slice.Slice) for a in b]) for b in self.bitmap])

    def printBitmap(self):
        print("Bitmap")
        for row in self.bitmap:
            print([ "n " if not isinstance(el,Slice.Slice) else "{} ".format(self.seedSlice.index(el)) for el in row])

    #def delPiùBrutto(self):
    def unupdateBitmap(self, slice):
        for x in range(slice.fro[0],slice.to[0]):
            for y in range(slice.fro[1],slice.to[1]):
                self.bitmap[x,y] = 0

    def getValidSlice(self):
        return  [ el for el in pizza.seedSlice if el.isValidForMinIngr()]

    def getAreaValidSlice(self):
        return sum(map(lambda  x : x.area, self.getValidSlice() ))

    def killNonValidSlice(self):
        if len([el for el in pizza.seedSlice if not el.isValidForMinIngr()])>0:
            el  = np.random.choice(a= np.asarray([el for el in pizza.seedSlice if not el.isValidForMinIngr()]))
            #print(el)
            self.unupdateBitmap(el)
            self.seedSlice.remove(el)
            #print("Rimosso {} conferma: {}".format(el,not el in self.seedSlice))


if __name__ == "__main__":
    outputFolder = "dati_out/"
    file = sys.argv[1]
    fileName = re.sub(".+/","",file)

    pizza = getPizza(file)
    lastFailingIteration = 0
    bestAreaSlice = 0
    bestPizza = pizza
    iteration = int(np.sqrt(pizza.col_count * pizza.row_count )) + 10
    givupThreshold = (int(np.sqrt(pizza.col_count * pizza.row_count )) + 10)//10
    print("Start with {} iteration, givupThreshold: {}".format(iteration,givupThreshold))

    while iteration > 0:
        try:
            iteration-=1
            pizza.step()
            currAreaSlice = pizza.getAreaValidSlice()
            #print("iteration {} currArea: {}".format(iteration,currAreaSlice))
            if bestAreaSlice < currAreaSlice:
                bestPizza = pizza
                bestAreaSlice = currAreaSlice

        except BaseException:
            #print("Non miglioro più")
            if iteration+givupThreshold==lastFailingIteration:
                #break
                pizza = getPizza(file)
            lastFailingIteration = iteration
            currAreaSlice = pizza.getAreaValidSlice()
            if bestAreaSlice<currAreaSlice:
                bestPizza=pizza
                bestAreaSlice=currAreaSlice

            pizza.killNonValidSlice()
            #print(pizza.getAreaValidSlice())

        #print(bestPizza.printBitmap())
        #print("Pizza score: {} Missing pieces: {}".format(bestAreaSlice, bestPizza.tot_el - bestAreaSlice))

        #print(len(bestPizza.seedSlice))
        #print("\n".join(  [ "{} {} {} {}".format(el.fro[0],el.fro[1],el.to[0],el.to[1]) for el in bestPizza.seedSlice if el.isValidForMinIngr() ] ))
        with open(outputFolder + fileName + ".out","w",encoding="utf8") as f:
            f.write("{}\n".format(len(bestPizza.seedSlice)))
            f.write("\n".join(  [ "{} {} {} {}".format(el.fro[0],el.fro[1],el.to[0]-1,el.to[1]-1) for el in bestPizza.seedSlice if el.isValidForMinIngr() ] ))
            f.close()


